package org.template.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.template.components.ContactForm;
import org.template.components.Header;

import java.util.stream.Stream;


public class FillInAndSendContactFormTest extends AbstractTest {

    Header header = new Header(driver);

    @Test
    public void shouldNavigateToContactForm() {
        header.goToContactForm();

        Assertions.assertEquals("Contact", header.getLastBreadcrumbText());
    }

    @ParameterizedTest
    @MethodSource("requiredFieldsData")
    public void shouldEmailBeRequired(String email, String message, String alert) {
        ContactForm contactForm = header.goToContactForm();
        contactForm.fillEmail(email);
        contactForm.fillMessage(message);
        contactForm.clickSend();

        Assertions.assertTrue(contactForm.getErrorMessage().contains(alert));
    }

    private static Stream<Arguments> requiredFieldsData() {
        return Stream.of(
                Arguments.of("", "mess", "Invalid email address."),                 //to verify if email is required
                Arguments.of("xxx", "mess", "Invalid email address."),              //to verify if regex for email is checked
                Arguments.of("test@softie.pl", "", "The message cannot be blank."), //to verify if message is required
                Arguments.of("test@softie.pl", "1.test\n2.test\n3.ttt test teee",
                        "Please select a subject from the list provided.")          //to verify if subject is required
        );
    }

    @Test
    public void shouldSendMessageWhenEmailSubjectAndMessageAreFilledCorrectly() {
        ContactForm contactForm = header.goToContactForm();
        contactForm.fillEmail("test@softie.pl");
        contactForm.fillMessage("1.test\n2.test\n3.treeetertetertrete ertetert rf");
        contactForm.clickCustomerServiceFromSubjectCombo();
        contactForm.clickSend();

        Assertions.assertEquals("Your message has been successfully sent to our team.", contactForm.getInfoMessage());
    }

    @Test
    public void shouldSendMessageWithAttachement() {
        ContactForm contactForm = header.goToContactForm();
        contactForm.fillEmail("test@softie.pl");
        contactForm.fillMessage("1.test\n2.test\n3.treeetertetertrete ertetert rf");
        contactForm.clickCustomerServiceFromSubjectCombo();
        String txtFileNameFromResources = "msgAttachment.txt";
        contactForm.chooseFile(txtFileNameFromResources);

        Assertions.assertEquals(txtFileNameFromResources, contactForm.getAttachedFileName());
        contactForm.clickSend();
        Assertions.assertEquals("Your message has been successfully sent to our team.", contactForm.getInfoMessage());
    }

}
