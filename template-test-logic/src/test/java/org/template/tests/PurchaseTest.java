package org.template.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.template.components.*;
import org.template.model.User;
import org.template.pageobjects.CartPage;
import org.template.pageobjects.SearchingResultsPage;

public class PurchaseTest extends AbstractTest {

    Header header = new Header(driver);
    User user = testData.getUsersData().get(0);

    @Test
    public void buyFirstFoundProductAsLoggedUserTest() {
        addSessionCookieToBrowserOf(user);
        driver.navigate().refresh();
        SearchingResultsPage searchingResultsPage = header.searchByKeyword("dress");
        searchingResultsPage.addFirstProductToCart();
        ProductAddedPopup productAddedPopup = searchingResultsPage.getProductAddedPopup(driver);
        CartPage cartPage= productAddedPopup.clickProceedToCheckout()
                .clickProceedToCheckoutOn1stStep()
                .clickProceedToCheckoutOn3rdStep()
                .clickOnAgreementCheckbox()
                .clickProceedToCheckoutOn4thStep()
                .clickOnPayByBankWireTile()
                .clickConfirm();
        String breadcrumb = cartPage.getHeader().getLastBreadcrumbText();

        Assertions.assertEquals("Order confirmation", breadcrumb);
    }
}
