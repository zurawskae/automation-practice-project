package org.template.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.template.pageobjects.HomePage;
import org.template.pageobjects.SearchingResultsPage;

import java.util.stream.Stream;

public class SearchTest extends AbstractTest {

    HomePage homePage = new HomePage(driver);

    @ParameterizedTest
    @MethodSource("positiveSearchData")
    public void searchWithResults(String keyword, String expectedPageHeaderSubstring) {
        SearchingResultsPage searchingResultsPage = homePage.getHeader()
                .searchByKeyword(keyword);

        Assertions.assertTrue(searchingResultsPage.getPageHeaderText().contains(expectedPageHeaderSubstring));
    }

    @ParameterizedTest
    @MethodSource("negativeSearchData")
    public void searchWithoutResults(String keyword, String expectedMessage) {
        SearchingResultsPage searchingResultsPage = homePage.getHeader()
                .searchByKeyword(keyword);

        Assertions.assertEquals(expectedMessage, searchingResultsPage.getAlertText());
    }

    private static Stream<Arguments> positiveSearchData() {
        return Stream.of(
                Arguments.of("dress", "SEARCH  \"DRESS"),
                Arguments.of("BLOUSE", "SEARCH  \"BLOUSE"),
                Arguments.of("Shirt", "SEARCH  \"SHIRT")
        );
    }

    private static Stream<Arguments> negativeSearchData() {
        return Stream.of(
                Arguments.of("xyzzzz", "No results were found for your search \"xyzzzz\""),
                Arguments.of("BLOUSEbb", "No results were found for your search \"BLOUSEbb\"")
        );
    }

}
