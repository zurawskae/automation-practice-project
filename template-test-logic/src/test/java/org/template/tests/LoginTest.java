package org.template.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.template.components.LoginForm;
import org.template.model.User;
import org.template.pageobjects.HomePage;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;

public class LoginTest extends AbstractTest{

    User user = testData.getUsersData().get(0);

    @Test
    public void shuoldSuccessfullyLogInUser() {
        HomePage homePage = new HomePage(driver);
        homePage.getHeader()
                .goToLogInPage()
                .getLoginForm()
                .fillAndSendLoginForm(user.getEmail(), user.getPassword());

        Assertions.assertTrue(homePage.getHeader().isSignOutButtonDisplayed());
    }

    @ParameterizedTest
    @MethodSource("loginFormData")
    public void shouldNotLoginWithIncorrectCredentials(String user, String pass, String expectedErrorMessage) {
        HomePage homePage = new HomePage(driver);
        LoginForm loginForm = homePage.getHeader()
                .goToLogInPage()
                .getLoginForm()
                .fillAndSendLoginForm(user, pass);

        Assertions.assertTrue(loginForm.getErrorMessage().contains(expectedErrorMessage));
    }
    private static Stream<Arguments> loginFormData() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return Stream.of(
                Arguments.of("test@softie.pl", "wrong Password", "Authentication failed."),
                Arguments.of("test@softie.pl", "", "Password is required."),
                Arguments.of("", "wrong Password", "An email address required."),
                Arguments.of("", "", "An email address required."),
                Arguments.of("test@@"+sdf.format(timestamp)+"@test.com", "wrong_password", "Invalid email address.")
        );
    }
}
