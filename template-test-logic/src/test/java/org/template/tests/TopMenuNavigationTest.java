package org.template.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.template.components.Header;

import java.util.stream.Stream;

public class TopMenuNavigationTest extends AbstractTest {

    @ParameterizedTest
    @MethodSource("topMenuSuboptions")
    public void shuoldOpenCardForTopMenuSuboption(String topMenuTale, String suboption) {
        Header header = new Header(driver);
        header.clickSuboptionFromTopMenu(topMenuTale, suboption);

        Assertions.assertTrue(suboption.trim().equalsIgnoreCase(header.getLastBreadcrumbText()));
    }

    private static Stream<Arguments> topMenuSuboptions() {
        return Stream.of(
                Arguments.of("Women", "Tops"),
                Arguments.of("Women", "T-shirts"),
                Arguments.of("Women", "Blouses"),
                Arguments.of("Women", "Dresses"),
                Arguments.of("Women", "Evening Dresses"),
                Arguments.of("Dresses", "Casual Dresses"),
                Arguments.of("Dresses", "Evening Dresses"),
                Arguments.of("Dresses", "Summer Dresses")
        );
    }

    @ParameterizedTest
    @MethodSource("topMenuOptions")
    public void shouldOpenCardForTopMenuOption(String topMenuTale) {
        Header header = new Header(driver);
        header.clickMainOptionFromTopMenu(topMenuTale);

        Assertions.assertEquals(topMenuTale, header.getLastBreadcrumbText());
    }

    private static Stream<Arguments> topMenuOptions() {
        return Stream.of(
                Arguments.of("Women"),
                Arguments.of("Dresses"),
                Arguments.of("T-shirts")
        );
    }

}
