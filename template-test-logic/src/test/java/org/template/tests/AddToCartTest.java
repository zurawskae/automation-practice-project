package org.template.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.template.components.Header;
import org.template.components.ProductAddedPopup;
import org.template.pageobjects.SearchingResultsPage;

public class AddToCartTest extends AbstractTest {

    Header header = new Header(driver);

    @Test
    public void addFirstFoundProductToCartTest() {
        SearchingResultsPage searchingResultsPage = header.searchByKeyword("dress");
        searchingResultsPage.addFirstProductToCart();
        ProductAddedPopup productAddedPopup = searchingResultsPage.getProductAddedPopup(driver);
        String confirmationMessage = productAddedPopup.getConfirmationHeaderText();
        Assertions.assertEquals("Product successfully added to your shopping cart", confirmationMessage);
    }
}
