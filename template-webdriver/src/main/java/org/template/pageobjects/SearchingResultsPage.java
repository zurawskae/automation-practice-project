package org.template.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.template.AbstractPage;
import org.template.components.Header;
import org.template.components.ProductAddedPopup;

import java.time.Duration;
import java.util.List;

public class SearchingResultsPage extends AbstractPage {

    @FindBy(css = "h1")
    private WebElement pageHeader;
    @FindBy(className = "alert")
    private WebElement alert;
    @FindBy(className = "ajax_block_product")
    private List<WebElement> products;
    @FindBy(id = "layer_cart")
    private WebElement popup;

    public SearchingResultsPage(WebDriver driver) {
        super(driver);
    }

    public Header getHeader() {
        return new Header(driver);
    }

    public ProductAddedPopup getProductAddedPopup(WebDriver driver) {
        return new ProductAddedPopup(driver);
    }

    public String getPageHeaderText() {
        return pageHeader.getText();
    }

    public String getAlertText() {
        return alert.getText();
    }

    public WebElement getFirstProduct() {
        return products.get(0);
    }

    public void addFirstProductToCart() {
        WebElement firstProduct = getFirstProduct();
        Actions actions = new Actions(driver);
        actions.moveToElement(firstProduct).perform();
        WebElement addToCartButton = firstProduct.findElement(By.className("ajax_add_to_cart_button"));
        addToCartButton.click();
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.visibilityOf(popup));
    }
}
