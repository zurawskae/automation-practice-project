package org.template.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.template.AbstractPage;
import org.template.components.*;

public class CartPage extends AbstractPage {

    @FindBy(linkText = "Proceed to checkout")
    private WebElement proceedToCheckout1stStepButton;
    @FindBy(css = "button[name='processAddress']")
    private WebElement proceedToCheckout3rdStepButton;
    @FindBy(id = "cgv")
    private WebElement agreementCheckbox;
    @FindBy(css = "button[name='processCarrier']")
    private WebElement proceedToCheckout4thStepButton;
    @FindBy(css = "a.bankwire")
    private WebElement payByBankWireTile;
    @FindBy(id = "cart_navigation")
    private WebElement previousAndNextSection;

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public Header getHeader() {
        return new Header(driver);
    }

    public CartPage clickProceedToCheckoutOn1stStep() {
        proceedToCheckout1stStepButton.click();
        return this;
    }

    public CartPage clickProceedToCheckoutOn3rdStep() {
        proceedToCheckout3rdStepButton.click();
        return this;
    }

    public CartPage clickOnAgreementCheckbox() {
        agreementCheckbox.click();
        return this;
    }

    public CartPage clickProceedToCheckoutOn4thStep() {
        proceedToCheckout4thStepButton.click();
        return this;
    }

    public CartPage clickOnPayByBankWireTile() {
        payByBankWireTile.click();
        return this;
    }

    public CartPage clickConfirm() {
        previousAndNextSection.findElement(By.cssSelector("button")).click();
        return this;
    }

}
