package org.template.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.template.AbstractPage;
import org.template.pageobjects.LoginPage;
import org.template.pageobjects.SearchingResultsPage;

public class Header extends AbstractPage {

    @FindBy(css = "a.login")
    private WebElement singInButton;
    @FindBy(linkText = "Sign out")
    private WebElement signOutButton;
    @FindBy(linkText = "Contact us")
    private WebElement contactUsButton;
    @FindBy(css = ".search_query")
    private WebElement searchBox;
    @FindBy(css = "[name=\"submit_search\"]")
    private WebElement magnifier;
    @FindBy(css = ".sf-menu")
    private WebElement topMenu;
    @FindBy(className = "breadcrumb")
    private WebElement breadcrumbs;

    WebDriverWait webDriverWait;

    public Header(WebDriver driver) {
        super(driver);
    }

    public LoginPage goToLogInPage() {
        singInButton.click();
        return new LoginPage(driver);
    }

    public ContactForm goToContactForm() {
        contactUsButton.click();
        return new ContactForm(driver);
    }

    public WebElement getMainOptionFromTopMenu(String option) {                     //options: Women, Dresses, T-Shirts
        option = option.trim();
        option = option.substring(0, 1).toUpperCase() + option.substring(1).toLowerCase();
        return driver.findElement(By.cssSelector(".sf-menu>li>a[title='" + option + "']"));
    }

    public void clickMainOptionFromTopMenu(String option) {
        getMainOptionFromTopMenu(option).click();
    }

    public void moveToMainOptionFromTopMenu(String option) {
        WebElement tale = getMainOptionFromTopMenu(option);
        Actions builder = new Actions(driver);
        builder.moveToElement(tale).build().perform();
    }

    public WebElement getSuboptionFromTopMenu(String suboption) {                   //options from all sublevels: Tops, T-shirts, Blouses, etc.
        suboption = suboption.trim();
        return driver.findElement(By.cssSelector(".submenu-container[style='display: block;'] a[title='" + suboption + "']"));
    }

    public void clickSuboptionFromTopMenu(String option, String suboption) {
        moveToMainOptionFromTopMenu(option);
        getSuboptionFromTopMenu(suboption).click();
    }

    public String getLastBreadcrumbText() {
        String[] arr = breadcrumbs.getText().trim().split(">");
        String str = arr[arr.length - 1];
        return str.trim();
    }

    public SearchingResultsPage searchByKeyword(String keyword) {
        searchBox.sendKeys(keyword);
        magnifier.click();
        return new SearchingResultsPage(driver);
    }

    public void clickSignOutButton() {
        signOutButton.click();
    }

    public boolean isSignInButtonDisplayed() {
        return singInButton.isDisplayed();
    }

    public boolean isSignOutButtonDisplayed() {
        return signOutButton.isDisplayed();
    }

}
