package org.template.components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.template.AbstractPage;
import org.template.pageobjects.CartPage;

public class ProductAddedPopup extends AbstractPage {

    @FindBy(xpath = "//div[contains(@class, 'layer_cart_product')]/h2")
    private WebElement confirmationHeader;
    @FindBy(linkText = "Proceed to checkout")
    private WebElement proceedToCheckoutButton;

    public ProductAddedPopup(WebDriver driver) {
        super(driver);
    }

    public String getConfirmationHeaderText() {
        return confirmationHeader.getText();
    }

    public CartPage clickProceedToCheckout() {
        proceedToCheckoutButton.click();
        return new CartPage(driver);
    }

}
