package org.template.components;

import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.template.AbstractPage;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;
import java.util.List;

import static java.util.Objects.requireNonNull;


public class ContactForm extends AbstractPage {

    @FindBy(id = "id_contact")
    private WebElement subjectCombobox;
    @FindBy(id = "email")
    private WebElement emailInput;
    @FindBy(css = "span.filename")
    private WebElement fileNameSpan;
    @FindBy(id = "uniform-fileUpload")
    private WebElement chooseFileSection;
    @FindBy(id = "message")
    private WebElement messageTextarea;
    @FindBy(id = "submitMessage")
    private WebElement sendButton;
    @FindBy(className = "alert-danger")
    private WebElement alertElement;
    @FindBy(className = "alert-success")
    private WebElement infoElement;
    @FindBy(id = "fileUpload")
    private WebElement fileUploadInput;
    @FindBy(css = "#id_contact option")
    private List<WebElement> comboOptions;

    public ContactForm(WebDriver driver) {
        super(driver);
    }

    public String getErrorMessage() {
        return alertElement.getText();
    }

    public String getInfoMessage() {
        return infoElement.getText();
    }

    public void clickSend() {
        sendButton.click();
    }

    public void fillEmail(String email) {
        if (email != null) emailInput.sendKeys(email);
    }

    public void fillMessage(String msg) {
        messageTextarea.sendKeys(msg);
    }

    public List<WebElement> getSubjectComboboxOptions() {
        return subjectCombobox.findElements(By.cssSelector("option"));
    }

    public void clickCustomerServiceFromSubjectCombo() {
        comboOptions.get(1).click();
    }

    public void clickWebmasterFromSubjectCombo() {
        comboOptions.get(2).click();
    }

    public void clearSubjectCombo() {
        comboOptions.get(0).click();
    }

    public void clickChooseFile() {
        chooseFileSection.click();
    }

    @SneakyThrows
    public void chooseFile(String fileNameFromResources) {
        URL url = getClass().getClassLoader().getResource(fileNameFromResources);
        requireNonNull(url, "Cannot find resource file: " + fileNameFromResources);
        String filePath = new File(url.toURI()).getAbsolutePath();
        fileUploadInput.sendKeys(filePath);
    }

    public String getAttachedFileName() {
        return fileNameSpan.getText();
    }

}
