# README #

Project created for learning automation tests with http://automationpractice.com as tested website.

### What is this repository for? ###

* Project created for learning automation tests with JAVA and Selenium Webdriver
* [Repository link](https://bitbucket.org/zurawskae/automation-practice-project/src/master/)

### Technologies used in this project ###
* Java
* Maven
* Selenium WebDriver
* jUnit
* Page Object Pattern
* Rest Assured

### How to change a browser used in this tests ###
1. go to Configuration class 
2. Set 'def' value for BROWSER as below:
   1. ="chrome" when you want to run tests on ChromeDriver
   2. ="firefox" when you want to run tests on GeckoDriver
   3. ="ie" when you want to run tests on InternetExplorerDriver
   4. ="safari" when you want to run tests with using SafariDriver
   
### Requirements ###
* At least one of the following browsers: Chrome, Firefox, IE, Safari
* appropriate driver for your browser
* maven

### How to run tests ###

* download the project
* open terminal for automation-practice-project directory
* type "mvn test" into terminal and press ENTER - it will compile the code and run all tests